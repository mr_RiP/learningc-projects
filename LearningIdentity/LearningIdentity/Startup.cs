﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LearningIdentity.Startup))]
namespace LearningIdentity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
